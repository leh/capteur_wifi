#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <DHT_U.h>
#include <DHT.h>
#include "DHT.h"
#define DHTPIN 23
#define DHTTYPE DHT11

WiFiMulti WiFiMulti;
HTTPClient http;
DHT sensor(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(115200);
  sensor.begin();
  delay(10);
  WiFiMulti.addAP("POP_SENSORS", "P0PS3NS0RS!");
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  pinMode(21, OUTPUT);
  pinMode(22, OUTPUT);
}

void loop() {
  delay(300);
  float h = sensor.readHumidity(); //reads the humiditiy
  float t = sensor.readTemperature(); //reads the temp
  //sends humidity and temp to Serial port.
  Serial.print("H:");
  Serial.print(h);
  Serial.print(",");
  Serial.print("T:");
  Serial.println(t);


  //host to contact
  String host = "http://node.blenderlab.fr";
  //URL to get
  String url = "/sensor/g08?temp=" + String(t) + "&hygro=" + String(h) + "&label=Louise_ClassRM";
  http.begin(host + url);
  int retCODE = http.GET();
  Serial.print("[HTTP] Return code =");
  Serial.println(retCODE);
  // All is working
  if (retCODE > 0) {

    if (retCODE == HTTP_CODE_OK) {
      //all is ok green led comes on
      digitalWrite(21, HIGH);
      digitalWrite (22, LOW);
      String content = http.getString();
      Serial.println(content);
    }
    else {
      Serial.println("[HTTP]: Ooops.... Something's not right");
      digitalWrite(21, LOW);
      digitalWrite (22, HIGH);
    }//end IF Code OK
  } // If an error red led comes on
  else {
    Serial.print("[HTTP]GET failed, error:");
    Serial.println(retCODE);
    digitalWrite(21, LOW);
    digitalWrite (22, HIGH);

  }
  delay(600000);
}
